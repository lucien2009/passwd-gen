# PasswdGen

#### 介绍
C#编写的Winform应用程序，开发办公过程中常用到的工具：  
1. 密码生成器；  
2. GUID生成器;   
3. 中文拼音首字母/全拼-转换器
== 2023-6-19
4. 增加“身份证”“测试数据-人员姓名”生成功能 
5. 增加“md5”加密串功能

#### 日志
2021.11.10  提取首字母，或拼音，提供“大小写”选择功能

#### 软件架构
软件架构说明
C# vs2012


#### 效果图
![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE01.png)

![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE04.png)

![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE03.png)

![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE02.png)

![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE06.png)

![](https://gitee.com/lucien2009/passwd-gen/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE05.png)




#### blog地址

https://blog.csdn.net/coolhe21cn/article/details/108144842?spm=1001.2014.3001.5501

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
