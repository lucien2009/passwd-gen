﻿namespace PasswdGen
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chkNum = new System.Windows.Forms.CheckBox();
            this.chkWordLower = new System.Windows.Forms.CheckBox();
            this.chkWordUpper = new System.Windows.Forms.CheckBox();
            this.tbBuildNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBuild = new System.Windows.Forms.Button();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lstPwdList = new System.Windows.Forms.ListBox();
            this.btnCopySelect = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtPasswdLength = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_passwdGen = new System.Windows.Forms.TabPage();
            this.txtPwdList = new System.Windows.Forms.TextBox();
            this.tp_guid = new System.Windows.Forms.TabPage();
            this.rTxtGuid = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLength = new System.Windows.Forms.Label();
            this.btnGuidGen = new System.Windows.Forms.Button();
            this.cbxUpper = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNum = new System.Windows.Forms.TextBox();
            this.txtSpliter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tp_pinyin = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rTxtHanzi = new System.Windows.Forms.RichTextBox();
            this.rTxtPinyin = new System.Windows.Forms.RichTextBox();
            this.pnlTop_tp3 = new System.Windows.Forms.Panel();
            this.chk_lowercase = new System.Windows.Forms.CheckBox();
            this.rbSzm = new System.Windows.Forms.RadioButton();
            this.rbQp = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.btnZh = new System.Windows.Forms.Button();
            this.tp_testdata = new System.Windows.Forms.TabPage();
            this.tc_testdata = new System.Windows.Forms.TabControl();
            this.tp_testdata_sql = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pnl_testdata_sql_top = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtSjqj_end = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtScsl = new System.Windows.Forms.TextBox();
            this.txtSjqj_start = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cbx_age = new System.Windows.Forms.CheckBox();
            this.cbx_idcode = new System.Windows.Forms.CheckBox();
            this.cbx_sex = new System.Windows.Forms.CheckBox();
            this.btn_genName = new System.Windows.Forms.Button();
            this.txt_testData_xm_gennum = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_testdata_genname = new System.Windows.Forms.TextBox();
            this.tp_md5 = new System.Windows.Forms.TabPage();
            this.txtMd5 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInputStr = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_gen_md5 = new System.Windows.Forms.Button();
            this.tp_about = new System.Windows.Forms.TabPage();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rdbtnGuid = new System.Windows.Forms.RadioButton();
            this.rdbtnXhID = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.tp_passwdGen.SuspendLayout();
            this.tp_guid.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tp_pinyin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnlTop_tp3.SuspendLayout();
            this.tp_testdata.SuspendLayout();
            this.tc_testdata.SuspendLayout();
            this.tp_testdata_sql.SuspendLayout();
            this.pnl_testdata_sql_top.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tp_md5.SuspendLayout();
            this.tp_about.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkNum
            // 
            this.chkNum.AutoSize = true;
            this.chkNum.Checked = true;
            this.chkNum.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNum.Location = new System.Drawing.Point(34, 18);
            this.chkNum.Name = "chkNum";
            this.chkNum.Size = new System.Drawing.Size(48, 16);
            this.chkNum.TabIndex = 0;
            this.chkNum.Text = "数字";
            this.chkNum.UseVisualStyleBackColor = true;
            this.chkNum.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chkWordLower
            // 
            this.chkWordLower.AutoSize = true;
            this.chkWordLower.Checked = true;
            this.chkWordLower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWordLower.Location = new System.Drawing.Point(91, 18);
            this.chkWordLower.Name = "chkWordLower";
            this.chkWordLower.Size = new System.Drawing.Size(72, 16);
            this.chkWordLower.TabIndex = 1;
            this.chkWordLower.Text = "小写字母";
            this.chkWordLower.UseVisualStyleBackColor = true;
            this.chkWordLower.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chkWordUpper
            // 
            this.chkWordUpper.AutoSize = true;
            this.chkWordUpper.Checked = true;
            this.chkWordUpper.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkWordUpper.Location = new System.Drawing.Point(171, 17);
            this.chkWordUpper.Name = "chkWordUpper";
            this.chkWordUpper.Size = new System.Drawing.Size(72, 16);
            this.chkWordUpper.TabIndex = 2;
            this.chkWordUpper.Text = "大写字母";
            this.chkWordUpper.UseVisualStyleBackColor = true;
            this.chkWordUpper.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // tbBuildNum
            // 
            this.tbBuildNum.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbBuildNum.Location = new System.Drawing.Point(141, 49);
            this.tbBuildNum.Name = "tbBuildNum";
            this.tbBuildNum.Size = new System.Drawing.Size(102, 29);
            this.tbBuildNum.TabIndex = 3;
            this.tbBuildNum.Text = "6";
            this.tbBuildNum.TextChanged += new System.EventHandler(this.tbBuildNum_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(36, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "生成数量：";
            // 
            // btnBuild
            // 
            this.btnBuild.Location = new System.Drawing.Point(579, 17);
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.Size = new System.Drawing.Size(119, 35);
            this.btnBuild.TabIndex = 5;
            this.btnBuild.Text = "开始生成";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(579, 65);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(119, 35);
            this.btnSaveAs.TabIndex = 6;
            this.btnSaveAs.Text = "保存";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // lstPwdList
            // 
            this.lstPwdList.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lstPwdList.FormattingEnabled = true;
            this.lstPwdList.ItemHeight = 19;
            this.lstPwdList.Location = new System.Drawing.Point(34, 118);
            this.lstPwdList.Name = "lstPwdList";
            this.lstPwdList.Size = new System.Drawing.Size(351, 346);
            this.lstPwdList.TabIndex = 7;
            this.lstPwdList.SelectedIndexChanged += new System.EventHandler(this.lstPwdList_SelectedIndexChanged);
            this.lstPwdList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstPwdList_MouseDoubleClick);
            // 
            // btnCopySelect
            // 
            this.btnCopySelect.Location = new System.Drawing.Point(714, 17);
            this.btnCopySelect.Name = "btnCopySelect";
            this.btnCopySelect.Size = new System.Drawing.Size(119, 35);
            this.btnCopySelect.TabIndex = 10;
            this.btnCopySelect.Text = "复制选中项";
            this.btnCopySelect.UseVisualStyleBackColor = true;
            this.btnCopySelect.Click += new System.EventHandler(this.btnCopySelect_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(714, 65);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(119, 35);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "删除选中项";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtPasswdLength
            // 
            this.txtPasswdLength.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPasswdLength.Location = new System.Drawing.Point(366, 49);
            this.txtPasswdLength.Name = "txtPasswdLength";
            this.txtPasswdLength.Size = new System.Drawing.Size(115, 29);
            this.txtPasswdLength.TabIndex = 12;
            this.txtPasswdLength.Text = "8";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(261, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 19);
            this.label2.TabIndex = 13;
            this.label2.Text = "密码长度：";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp_passwdGen);
            this.tabControl1.Controls.Add(this.tp_guid);
            this.tabControl1.Controls.Add(this.tp_pinyin);
            this.tabControl1.Controls.Add(this.tp_testdata);
            this.tabControl1.Controls.Add(this.tp_md5);
            this.tabControl1.Controls.Add(this.tp_about);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(120, 30);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(869, 519);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 14;
            // 
            // tp_passwdGen
            // 
            this.tp_passwdGen.Controls.Add(this.txtPwdList);
            this.tp_passwdGen.Controls.Add(this.btnBuild);
            this.tp_passwdGen.Controls.Add(this.label2);
            this.tp_passwdGen.Controls.Add(this.chkNum);
            this.tp_passwdGen.Controls.Add(this.txtPasswdLength);
            this.tp_passwdGen.Controls.Add(this.chkWordLower);
            this.tp_passwdGen.Controls.Add(this.btnDelete);
            this.tp_passwdGen.Controls.Add(this.chkWordUpper);
            this.tp_passwdGen.Controls.Add(this.btnCopySelect);
            this.tp_passwdGen.Controls.Add(this.tbBuildNum);
            this.tp_passwdGen.Controls.Add(this.lstPwdList);
            this.tp_passwdGen.Controls.Add(this.label1);
            this.tp_passwdGen.Controls.Add(this.btnSaveAs);
            this.tp_passwdGen.Location = new System.Drawing.Point(4, 34);
            this.tp_passwdGen.Name = "tp_passwdGen";
            this.tp_passwdGen.Padding = new System.Windows.Forms.Padding(3);
            this.tp_passwdGen.Size = new System.Drawing.Size(861, 481);
            this.tp_passwdGen.TabIndex = 0;
            this.tp_passwdGen.Text = "密码生成器";
            this.tp_passwdGen.UseVisualStyleBackColor = true;
            // 
            // txtPwdList
            // 
            this.txtPwdList.BackColor = System.Drawing.Color.White;
            this.txtPwdList.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPwdList.Location = new System.Drawing.Point(391, 117);
            this.txtPwdList.Multiline = true;
            this.txtPwdList.Name = "txtPwdList";
            this.txtPwdList.ReadOnly = true;
            this.txtPwdList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPwdList.Size = new System.Drawing.Size(462, 350);
            this.txtPwdList.TabIndex = 14;
            // 
            // tp_guid
            // 
            this.tp_guid.Controls.Add(this.rTxtGuid);
            this.tp_guid.Controls.Add(this.panel1);
            this.tp_guid.Location = new System.Drawing.Point(4, 34);
            this.tp_guid.Name = "tp_guid";
            this.tp_guid.Padding = new System.Windows.Forms.Padding(3);
            this.tp_guid.Size = new System.Drawing.Size(861, 481);
            this.tp_guid.TabIndex = 1;
            this.tp_guid.Text = "ID生成器";
            this.tp_guid.UseVisualStyleBackColor = true;
            // 
            // rTxtGuid
            // 
            this.rTxtGuid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rTxtGuid.Location = new System.Drawing.Point(3, 81);
            this.rTxtGuid.Name = "rTxtGuid";
            this.rTxtGuid.Size = new System.Drawing.Size(855, 397);
            this.rTxtGuid.TabIndex = 0;
            this.rTxtGuid.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdbtnXhID);
            this.panel1.Controls.Add(this.rdbtnGuid);
            this.panel1.Controls.Add(this.lblLength);
            this.panel1.Controls.Add(this.btnGuidGen);
            this.panel1.Controls.Add(this.cbxUpper);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtNum);
            this.panel1.Controls.Add(this.txtSpliter);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(855, 78);
            this.panel1.TabIndex = 7;
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(240, 49);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(41, 12);
            this.lblLength.TabIndex = 7;
            this.lblLength.Text = "长度:0";
            // 
            // btnGuidGen
            // 
            this.btnGuidGen.Location = new System.Drawing.Point(558, 38);
            this.btnGuidGen.Name = "btnGuidGen";
            this.btnGuidGen.Size = new System.Drawing.Size(105, 34);
            this.btnGuidGen.TabIndex = 1;
            this.btnGuidGen.Text = "Guid 生成 (&G)";
            this.btnGuidGen.UseVisualStyleBackColor = true;
            this.btnGuidGen.Click += new System.EventHandler(this.btnGuidGen_Click);
            // 
            // cbxUpper
            // 
            this.cbxUpper.AutoSize = true;
            this.cbxUpper.Checked = true;
            this.cbxUpper.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxUpper.Location = new System.Drawing.Point(172, 47);
            this.cbxUpper.Name = "cbxUpper";
            this.cbxUpper.Size = new System.Drawing.Size(48, 16);
            this.cbxUpper.TabIndex = 6;
            this.cbxUpper.Text = "大写";
            this.cbxUpper.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "分隔符：";
            // 
            // txtNum
            // 
            this.txtNum.Location = new System.Drawing.Point(464, 45);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(67, 21);
            this.txtNum.TabIndex = 5;
            this.txtNum.Text = "50";
            // 
            // txtSpliter
            // 
            this.txtSpliter.Location = new System.Drawing.Point(84, 45);
            this.txtSpliter.Name = "txtSpliter";
            this.txtSpliter.Size = new System.Drawing.Size(67, 21);
            this.txtSpliter.TabIndex = 3;
            this.txtSpliter.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(393, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "生成数量：";
            // 
            // tp_pinyin
            // 
            this.tp_pinyin.Controls.Add(this.splitContainer1);
            this.tp_pinyin.Controls.Add(this.pnlTop_tp3);
            this.tp_pinyin.Location = new System.Drawing.Point(4, 34);
            this.tp_pinyin.Name = "tp_pinyin";
            this.tp_pinyin.Padding = new System.Windows.Forms.Padding(3);
            this.tp_pinyin.Size = new System.Drawing.Size(861, 481);
            this.tp_pinyin.TabIndex = 2;
            this.tp_pinyin.Text = "拼音首字母";
            this.tp_pinyin.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 44);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rTxtHanzi);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rTxtPinyin);
            this.splitContainer1.Size = new System.Drawing.Size(855, 434);
            this.splitContainer1.SplitterDistance = 429;
            this.splitContainer1.TabIndex = 1;
            // 
            // rTxtHanzi
            // 
            this.rTxtHanzi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rTxtHanzi.Location = new System.Drawing.Point(0, 0);
            this.rTxtHanzi.Name = "rTxtHanzi";
            this.rTxtHanzi.Size = new System.Drawing.Size(429, 434);
            this.rTxtHanzi.TabIndex = 0;
            this.rTxtHanzi.Text = "";
            // 
            // rTxtPinyin
            // 
            this.rTxtPinyin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rTxtPinyin.Location = new System.Drawing.Point(0, 0);
            this.rTxtPinyin.Name = "rTxtPinyin";
            this.rTxtPinyin.Size = new System.Drawing.Size(422, 434);
            this.rTxtPinyin.TabIndex = 1;
            this.rTxtPinyin.Text = "";
            // 
            // pnlTop_tp3
            // 
            this.pnlTop_tp3.Controls.Add(this.chk_lowercase);
            this.pnlTop_tp3.Controls.Add(this.rbSzm);
            this.pnlTop_tp3.Controls.Add(this.rbQp);
            this.pnlTop_tp3.Controls.Add(this.label8);
            this.pnlTop_tp3.Controls.Add(this.btnZh);
            this.pnlTop_tp3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop_tp3.Location = new System.Drawing.Point(3, 3);
            this.pnlTop_tp3.Name = "pnlTop_tp3";
            this.pnlTop_tp3.Size = new System.Drawing.Size(855, 41);
            this.pnlTop_tp3.TabIndex = 0;
            // 
            // chk_lowercase
            // 
            this.chk_lowercase.AutoSize = true;
            this.chk_lowercase.Location = new System.Drawing.Point(343, 16);
            this.chk_lowercase.Name = "chk_lowercase";
            this.chk_lowercase.Size = new System.Drawing.Size(48, 16);
            this.chk_lowercase.TabIndex = 6;
            this.chk_lowercase.Text = "小写";
            this.chk_lowercase.UseVisualStyleBackColor = true;
            // 
            // rbSzm
            // 
            this.rbSzm.AutoSize = true;
            this.rbSzm.Checked = true;
            this.rbSzm.Location = new System.Drawing.Point(267, 15);
            this.rbSzm.Name = "rbSzm";
            this.rbSzm.Size = new System.Drawing.Size(59, 16);
            this.rbSzm.TabIndex = 5;
            this.rbSzm.TabStop = true;
            this.rbSzm.Text = "首字母";
            this.rbSzm.UseVisualStyleBackColor = true;
            // 
            // rbQp
            // 
            this.rbQp.AutoSize = true;
            this.rbQp.Location = new System.Drawing.Point(202, 15);
            this.rbQp.Name = "rbQp";
            this.rbQp.Size = new System.Drawing.Size(47, 16);
            this.rbQp.TabIndex = 4;
            this.rbQp.Text = "全拼";
            this.rbQp.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "转汉字首字母或全拼";
            // 
            // btnZh
            // 
            this.btnZh.Location = new System.Drawing.Point(424, 5);
            this.btnZh.Name = "btnZh";
            this.btnZh.Size = new System.Drawing.Size(100, 32);
            this.btnZh.TabIndex = 0;
            this.btnZh.Text = "转换->拼音";
            this.btnZh.UseVisualStyleBackColor = true;
            this.btnZh.Click += new System.EventHandler(this.btnZh_Click);
            // 
            // tp_testdata
            // 
            this.tp_testdata.Controls.Add(this.tc_testdata);
            this.tp_testdata.Location = new System.Drawing.Point(4, 34);
            this.tp_testdata.Name = "tp_testdata";
            this.tp_testdata.Padding = new System.Windows.Forms.Padding(3);
            this.tp_testdata.Size = new System.Drawing.Size(861, 481);
            this.tp_testdata.TabIndex = 4;
            this.tp_testdata.Text = "测试数据";
            this.tp_testdata.UseVisualStyleBackColor = true;
            // 
            // tc_testdata
            // 
            this.tc_testdata.Controls.Add(this.tp_testdata_sql);
            this.tc_testdata.Controls.Add(this.tabPage2);
            this.tc_testdata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_testdata.ItemSize = new System.Drawing.Size(96, 26);
            this.tc_testdata.Location = new System.Drawing.Point(3, 3);
            this.tc_testdata.Name = "tc_testdata";
            this.tc_testdata.SelectedIndex = 0;
            this.tc_testdata.Size = new System.Drawing.Size(855, 475);
            this.tc_testdata.TabIndex = 10;
            // 
            // tp_testdata_sql
            // 
            this.tp_testdata_sql.Controls.Add(this.richTextBox1);
            this.tp_testdata_sql.Controls.Add(this.pnl_testdata_sql_top);
            this.tp_testdata_sql.Location = new System.Drawing.Point(4, 30);
            this.tp_testdata_sql.Name = "tp_testdata_sql";
            this.tp_testdata_sql.Padding = new System.Windows.Forms.Padding(3);
            this.tp_testdata_sql.Size = new System.Drawing.Size(847, 441);
            this.tp_testdata_sql.TabIndex = 0;
            this.tp_testdata_sql.Text = "SQL插入模板";
            this.tp_testdata_sql.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(3, 79);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(841, 359);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // pnl_testdata_sql_top
            // 
            this.pnl_testdata_sql_top.Controls.Add(this.label12);
            this.pnl_testdata_sql_top.Controls.Add(this.textBox1);
            this.pnl_testdata_sql_top.Controls.Add(this.txtSjqj_end);
            this.pnl_testdata_sql_top.Controls.Add(this.label9);
            this.pnl_testdata_sql_top.Controls.Add(this.btnTest);
            this.pnl_testdata_sql_top.Controls.Add(this.checkBox3);
            this.pnl_testdata_sql_top.Controls.Add(this.label10);
            this.pnl_testdata_sql_top.Controls.Add(this.txtScsl);
            this.pnl_testdata_sql_top.Controls.Add(this.txtSjqj_start);
            this.pnl_testdata_sql_top.Controls.Add(this.label11);
            this.pnl_testdata_sql_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_testdata_sql_top.Location = new System.Drawing.Point(3, 3);
            this.pnl_testdata_sql_top.Name = "pnl_testdata_sql_top";
            this.pnl_testdata_sql_top.Size = new System.Drawing.Size(841, 76);
            this.pnl_testdata_sql_top.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(111, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 12);
            this.label12.TabIndex = 9;
            this.label12.Text = "-";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(7, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(449, 21);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "insert into <table>(col1, col2) values(val1,val2)；";
            // 
            // txtSjqj_end
            // 
            this.txtSjqj_end.Location = new System.Drawing.Point(131, 15);
            this.txtSjqj_end.Name = "txtSjqj_end";
            this.txtSjqj_end.Size = new System.Drawing.Size(39, 21);
            this.txtSjqj_end.TabIndex = 8;
            this.txtSjqj_end.Text = "9999";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(407, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 7;
            this.label9.Text = "长度:0";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(496, 12);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(95, 48);
            this.btnTest.TabIndex = 1;
            this.btnTest.Text = "生成 (&G)";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(339, 17);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(42, 16);
            this.checkBox3.TabIndex = 6;
            this.checkBox3.Text = "SQL";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "数据区间：";
            // 
            // txtScsl
            // 
            this.txtScsl.Location = new System.Drawing.Point(270, 15);
            this.txtScsl.Name = "txtScsl";
            this.txtScsl.Size = new System.Drawing.Size(47, 21);
            this.txtScsl.TabIndex = 5;
            this.txtScsl.Text = "10";
            // 
            // txtSjqj_start
            // 
            this.txtSjqj_start.Location = new System.Drawing.Point(76, 15);
            this.txtSjqj_start.Name = "txtSjqj_start";
            this.txtSjqj_start.Size = new System.Drawing.Size(28, 21);
            this.txtSjqj_start.TabIndex = 3;
            this.txtSjqj_start.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(203, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 4;
            this.label11.Text = "生成数量：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.cbx_age);
            this.tabPage2.Controls.Add(this.cbx_idcode);
            this.tabPage2.Controls.Add(this.cbx_sex);
            this.tabPage2.Controls.Add(this.btn_genName);
            this.tabPage2.Controls.Add(this.txt_testData_xm_gennum);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txt_testdata_genname);
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(847, 445);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "测试人信息";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(614, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 23;
            this.label16.Text = "年龄范围：";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox2.Location = new System.Drawing.Point(685, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(82, 29);
            this.textBox2.TabIndex = 22;
            this.textBox2.Text = "18-55";
            // 
            // cbx_age
            // 
            this.cbx_age.AutoSize = true;
            this.cbx_age.Location = new System.Drawing.Point(560, 20);
            this.cbx_age.Name = "cbx_age";
            this.cbx_age.Size = new System.Drawing.Size(48, 16);
            this.cbx_age.TabIndex = 21;
            this.cbx_age.Text = "年龄";
            this.cbx_age.UseVisualStyleBackColor = true;
            // 
            // cbx_idcode
            // 
            this.cbx_idcode.AutoSize = true;
            this.cbx_idcode.Location = new System.Drawing.Point(445, 43);
            this.cbx_idcode.Name = "cbx_idcode";
            this.cbx_idcode.Size = new System.Drawing.Size(72, 16);
            this.cbx_idcode.TabIndex = 20;
            this.cbx_idcode.Text = "身份证号";
            this.cbx_idcode.UseVisualStyleBackColor = true;
            // 
            // cbx_sex
            // 
            this.cbx_sex.AutoSize = true;
            this.cbx_sex.Location = new System.Drawing.Point(445, 21);
            this.cbx_sex.Name = "cbx_sex";
            this.cbx_sex.Size = new System.Drawing.Size(48, 16);
            this.cbx_sex.TabIndex = 19;
            this.cbx_sex.Text = "性别";
            this.cbx_sex.UseVisualStyleBackColor = true;
            // 
            // btn_genName
            // 
            this.btn_genName.Location = new System.Drawing.Point(296, 11);
            this.btn_genName.Name = "btn_genName";
            this.btn_genName.Size = new System.Drawing.Size(119, 35);
            this.btn_genName.TabIndex = 18;
            this.btn_genName.Text = "开始生成";
            this.btn_genName.UseVisualStyleBackColor = true;
            this.btn_genName.Click += new System.EventHandler(this.btn_genName_Click);
            // 
            // txt_testData_xm_gennum
            // 
            this.txt_testData_xm_gennum.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_testData_xm_gennum.Location = new System.Drawing.Point(132, 17);
            this.txt_testData_xm_gennum.Name = "txt_testData_xm_gennum";
            this.txt_testData_xm_gennum.Size = new System.Drawing.Size(102, 29);
            this.txt_testData_xm_gennum.TabIndex = 16;
            this.txt_testData_xm_gennum.Text = "20";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(27, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 19);
            this.label15.TabIndex = 17;
            this.label15.Text = "生成数量：";
            // 
            // txt_testdata_genname
            // 
            this.txt_testdata_genname.BackColor = System.Drawing.Color.White;
            this.txt_testdata_genname.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_testdata_genname.Location = new System.Drawing.Point(22, 70);
            this.txt_testdata_genname.Multiline = true;
            this.txt_testdata_genname.Name = "txt_testdata_genname";
            this.txt_testdata_genname.ReadOnly = true;
            this.txt_testdata_genname.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_testdata_genname.Size = new System.Drawing.Size(788, 350);
            this.txt_testdata_genname.TabIndex = 15;
            this.txt_testdata_genname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_testdata_genname_KeyDown);
            // 
            // tp_md5
            // 
            this.tp_md5.Controls.Add(this.txtMd5);
            this.tp_md5.Controls.Add(this.label14);
            this.tp_md5.Controls.Add(this.txtInputStr);
            this.tp_md5.Controls.Add(this.label13);
            this.tp_md5.Controls.Add(this.btn_gen_md5);
            this.tp_md5.Location = new System.Drawing.Point(4, 34);
            this.tp_md5.Name = "tp_md5";
            this.tp_md5.Padding = new System.Windows.Forms.Padding(3);
            this.tp_md5.Size = new System.Drawing.Size(861, 481);
            this.tp_md5.TabIndex = 5;
            this.tp_md5.Text = "加密";
            this.tp_md5.UseVisualStyleBackColor = true;
            // 
            // txtMd5
            // 
            this.txtMd5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMd5.Location = new System.Drawing.Point(20, 73);
            this.txtMd5.Multiline = true;
            this.txtMd5.Name = "txtMd5";
            this.txtMd5.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMd5.Size = new System.Drawing.Size(820, 395);
            this.txtMd5.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 3;
            this.label14.Text = "加密串：";
            // 
            // txtInputStr
            // 
            this.txtInputStr.Location = new System.Drawing.Point(93, 17);
            this.txtInputStr.Name = "txtInputStr";
            this.txtInputStr.Size = new System.Drawing.Size(378, 21);
            this.txtInputStr.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 12);
            this.label13.TabIndex = 1;
            this.label13.Text = "输入字符串：";
            // 
            // btn_gen_md5
            // 
            this.btn_gen_md5.Location = new System.Drawing.Point(526, 14);
            this.btn_gen_md5.Name = "btn_gen_md5";
            this.btn_gen_md5.Size = new System.Drawing.Size(75, 23);
            this.btn_gen_md5.TabIndex = 0;
            this.btn_gen_md5.Text = "加密 &G";
            this.btn_gen_md5.UseVisualStyleBackColor = true;
            this.btn_gen_md5.Click += new System.EventHandler(this.btn_gen_md5_Click);
            // 
            // tp_about
            // 
            this.tp_about.Controls.Add(this.linkLabel2);
            this.tp_about.Controls.Add(this.label7);
            this.tp_about.Controls.Add(this.label6);
            this.tp_about.Controls.Add(this.label5);
            this.tp_about.Controls.Add(this.linkLabel1);
            this.tp_about.Location = new System.Drawing.Point(4, 34);
            this.tp_about.Name = "tp_about";
            this.tp_about.Padding = new System.Windows.Forms.Padding(3);
            this.tp_about.Size = new System.Drawing.Size(861, 481);
            this.tp_about.TabIndex = 3;
            this.tp_about.Text = "关于";
            this.tp_about.UseVisualStyleBackColor = true;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(120, 160);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(101, 12);
            this.linkLabel2.TabIndex = 4;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "569461366@qq.com";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "Email：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "欢迎大家免费使用！";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "作者博客：";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(120, 132);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(197, 12);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "https://blog.csdn.net/coolhe21cn";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // rdbtnGuid
            // 
            this.rdbtnGuid.AutoSize = true;
            this.rdbtnGuid.Checked = true;
            this.rdbtnGuid.Location = new System.Drawing.Point(23, 19);
            this.rdbtnGuid.Name = "rdbtnGuid";
            this.rdbtnGuid.Size = new System.Drawing.Size(47, 16);
            this.rdbtnGuid.TabIndex = 10;
            this.rdbtnGuid.Text = "GUID";
            this.rdbtnGuid.UseVisualStyleBackColor = true;
            // 
            // rdbtnXhID
            // 
            this.rdbtnXhID.AutoSize = true;
            this.rdbtnXhID.Location = new System.Drawing.Point(395, 19);
            this.rdbtnXhID.Name = "rdbtnXhID";
            this.rdbtnXhID.Size = new System.Drawing.Size(59, 16);
            this.rdbtnXhID.TabIndex = 11;
            this.rdbtnXhID.Text = "雪花ID";
            this.rdbtnXhID.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 519);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "我的工具箱";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tp_passwdGen.ResumeLayout(false);
            this.tp_passwdGen.PerformLayout();
            this.tp_guid.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tp_pinyin.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnlTop_tp3.ResumeLayout(false);
            this.pnlTop_tp3.PerformLayout();
            this.tp_testdata.ResumeLayout(false);
            this.tc_testdata.ResumeLayout(false);
            this.tp_testdata_sql.ResumeLayout(false);
            this.pnl_testdata_sql_top.ResumeLayout(false);
            this.pnl_testdata_sql_top.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tp_md5.ResumeLayout(false);
            this.tp_md5.PerformLayout();
            this.tp_about.ResumeLayout(false);
            this.tp_about.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkNum;
        private System.Windows.Forms.CheckBox chkWordLower;
        private System.Windows.Forms.CheckBox chkWordUpper;
        private System.Windows.Forms.TextBox tbBuildNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ListBox lstPwdList;
        private System.Windows.Forms.Button btnCopySelect;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtPasswdLength;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_passwdGen;
        private System.Windows.Forms.TabPage tp_guid;
        private System.Windows.Forms.RichTextBox rTxtGuid;
        private System.Windows.Forms.Button btnGuidGen;
        private System.Windows.Forms.TextBox txtNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSpliter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbxUpper;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLength;
        private System.Windows.Forms.TabPage tp_pinyin;
        private System.Windows.Forms.Panel pnlTop_tp3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox rTxtHanzi;
        private System.Windows.Forms.RichTextBox rTxtPinyin;
        private System.Windows.Forms.Button btnZh;
        private System.Windows.Forms.TabPage tp_about;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbSzm;
        private System.Windows.Forms.RadioButton rbQp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tp_testdata;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel pnl_testdata_sql_top;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtScsl;
        private System.Windows.Forms.TextBox txtSjqj_start;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSjqj_end;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox chk_lowercase;
        private System.Windows.Forms.TextBox txtPwdList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TabPage tp_md5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtInputStr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_gen_md5;
        private System.Windows.Forms.TextBox txtMd5;
        private System.Windows.Forms.TabControl tc_testdata;
        private System.Windows.Forms.TabPage tp_testdata_sql;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_testdata_genname;
        private System.Windows.Forms.Button btn_genName;
        private System.Windows.Forms.TextBox txt_testData_xm_gennum;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox cbx_age;
        private System.Windows.Forms.CheckBox cbx_idcode;
        private System.Windows.Forms.CheckBox cbx_sex;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RadioButton rdbtnXhID;
        private System.Windows.Forms.RadioButton rdbtnGuid;
    }
}

