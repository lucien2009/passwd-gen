﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PasswdGen.Util;
using System.Deployment.Application;

namespace PasswdGen
{
    public partial class Form1 : Form
    {
        //随机数
        string rand = "";   

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 验证输入生成数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbBuildNum_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            if (!int.TryParse(tbBuildNum.Text.Trim(), out i))
            {
                MessageBox.Show("请输入大于0的任意数字！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                btnBuild.Enabled = false;
                tbBuildNum.Focus();
                tbBuildNum.SelectAll();
            }
            else if (i <= 0)
            {
                MessageBox.Show("请输入大于0的任意数字！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                btnBuild.Enabled = false;
                tbBuildNum.Focus();
                tbBuildNum.SelectAll();
            }
            else
                btnBuild.Enabled = true;
        }

        /// <summary>
        /// 开始生成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuild_Click(object sender, EventArgs e)
        {
            txtPwdList.Clear();
            string sfz_code = string.Empty;
            

            password_gen1();
            

            // txtPwdList.Text = password_gen();
            

            //MessageBox.Show(get_rand());
            //password_gen2();


        }

        /// <summary>
        /// 获取0-9，a-z，A-Z,_的字符串
        /// </summary>
        /// <returns></returns>
        private string get_rand()
        {
            rand = String.Empty;

            if (chkNum.Checked)
            {
                for (int i = 48; i <= 57; i++)
                {
                    rand += ((Char)i).ToString() ;
                }                
            }
            if (chkWordLower.Checked)
            {
                for (int i = 97; i <= 122; i++)
                {
                    rand += ((Char)i).ToString();
                }
            }
            if (chkWordUpper.Checked)
            {
                rand += "_";
            }
            if (chkWordUpper.Checked)
            {
                for (int i = 65; i <= 90; i++)
                {
                    rand += ((Char)i).ToString();
                }
            }

            return rand;
        }

        private void password_gen2() {
            Random r = new Random();
            string num = tbBuildNum.Text.Trim();

            string passwdLength = txtPasswdLength.Text.Trim();  // 密码长度

            string pwd = "";
            string pwd_all = String.Empty;


            for (int i = 0; i < Convert.ToInt32(num); i++)
            {

                for (int l = 0; l < Convert.ToInt32(passwdLength); l++)
                {
                    pwd += rand[r.Next(0, rand.Length)];
                }
                //pwd =  pwd + "\r\n";
                //MessageBox.Show(pwd);
                //sb.Append(pwd);
                txtPwdList.Text = pwd;

            }

            //txtPwdList.Text = pwd_all;
        }

        private void password_gen1()
        {

            lstPwdList.Items.Clear();
            rand = "";
            if (chkNum.Checked)
            {
                for (int i = 48; i <= 57; i++)
                {
                    rand += ((Char)i).ToString();
                }
            }
            if (chkWordLower.Checked)
            {
                for (int i = 97; i <= 122; i++)
                {
                    rand += ((Char)i).ToString();
                }
            }
            if (chkWordUpper.Checked)
            {
                rand += "_";
            }
            if (chkWordUpper.Checked)
            {
                for (int i = 65; i <= 90; i++)
                {
                    rand += ((Char)i).ToString();
                }
            }
            //btnBuild.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
        }


        /// <summary>
        /// 另存为文本文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string content = "";
                for (int i = 0; i < lstPwdList.Items.Count; i++)
                {
                    content += lstPwdList.GetItemText(lstPwdList.Items[i]) + "\r\n";
                }
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
                sw.Write(content);
                sw.Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            //comboBox1.SelectedIndex = 4;
        }

        /// <summary>
        /// 新线程生成密码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Random r = new Random();
            string num = tbBuildNum.Text.Trim();

            //string num1 = comboBox1.Text.Trim();
            string passwdLength = txtPasswdLength.Text.Trim();  // 密码长度

            string pwd = "";
            string sb = String.Empty;

            for (int i = 0; i < Convert.ToInt32(num); i++)
            {
                
                for (int l = 0; l < Convert.ToInt32(passwdLength); l++)
                {
                    pwd += rand[r.Next(0, rand.Length)];
                }
                lstPwdList.Items.Add(pwd);

                //MessageBox.Show(pwd);
                //sb.Append(pwd);
            }            

            ///////
            /// 
                       

            btnBuild.Enabled = true;
            btnSaveAs.Enabled = true;
            //progressBar1.Value = 100;
        }

        /// <summary>
        /// 复制选中项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCopySelect_Click(object sender, EventArgs e)
        {
            //if (lstPwdList.SelectedItems.Count > 0)
            //{
            //    Clipboard.SetText(lstPwdList.Text);
            //    MessageBox.Show("已复制到系统剪贴板！请用Ctrl+V 或 鼠标右键粘贴.", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

            txtPwdList.Clear();
            string sb = String.Empty;

            foreach (string item in this.lstPwdList.Items)
            {
                //MessageBox.Show(item);

                sb = sb + item.ToString() + "\r\n";
            }
            txtPwdList.Text = sb;
        }

        /// <summary>
        /// 删除选中项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            lstPwdList.Items.Remove(lstPwdList.SelectedItem);
        }

        /// <summary>
        /// 生成密码选中项更改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstPwdList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstPwdList.SelectedItem != null)
            {
                btnDelete.Enabled = true;
                btnCopySelect.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
                btnCopySelect.Enabled = false;
            }
        }

        /// <summary>
        /// 鼠标双击复制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstPwdList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lstPwdList.SelectedItems.Count > 0)
                btnCopySelect_Click(sender, new EventArgs());
        }

        /// <summary>
        /// 是否包含大写字母
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkWordLower.Checked && !chkNum.Checked && !chkWordUpper.Checked)
            {
                chkWordUpper.Checked = true;
            }
        }

        /// <summary>
        /// 是否包含小写字母
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkWordLower.Checked && !chkNum.Checked && !chkWordUpper.Checked)
            {
                chkWordLower.Checked = true;
            }
        }

        /// <summary>
        /// 是否包含数字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkWordLower.Checked && !chkNum.Checked && !chkWordUpper.Checked)
            {
                chkNum.Checked = true;
            }
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            Random r = new Random();
            for (int i = 0; i < Convert.ToInt32(tbBuildNum.Text.Trim()); i++)
            {
                string pwd = "";
                for (int l = 0; l < Convert.ToInt32(this.txtPasswdLength.Text.Trim()); l++)
                {
                    pwd += rand[r.Next(0, rand.Length)];
                }
                lstPwdList.Items.Add(pwd);
                //if (i > 0)
                //{
                //    backgroundWorker1.ReportProgress(Convert.ToInt32((i / Convert.ToDouble(tbBuildNum.Text.Trim())) * 100));
                //}
            }
            btnBuild.Enabled = true;
            btnSaveAs.Enabled = true;
            //progressBar1.Value = 100;
        }

        private void btnGuidGen_Click(object sender, EventArgs e)
        {
            rTxtGuid.Clear();
            if (this.rdbtnGuid.Checked) this.GuidGen();
            if (this.rdbtnXhID.Checked) this.XhidGen();
        }


        private void XhidGen()
        {
            SnowIdHelper idworker = SnowIdHelper.Singleton;
            //id = idworker.nextId();

            int num = Convert.ToInt32(this.txtNum.Text.Trim());

            string snowid = "";

            for (int i = 0; i < num; i++)
            {
                snowid = idworker.nextId().ToString();
                rTxtGuid.Text = rTxtGuid.Text + snowid + "\r\n";
            }

            this.lblLength.Text = String.Format("长度:{0}", snowid.Length.ToString());
        }


        /// <summary>
        /// 生成GUID
        /// </summary>
        private void GuidGen()
        {
            int num = Convert.ToInt32(this.txtNum.Text.Trim());

            string guid = "";

            for (int i = 0; i < num; i++)
            {
                guid = System.Guid.NewGuid().ToString().Replace("-", this.txtSpliter.Text.Trim()); // 默认或者去掉"-"

                if (cbxUpper.Checked)
                {
                    rTxtGuid.Text = rTxtGuid.Text + guid.ToUpper() + "\r\n";
                }
                else
                {
                    rTxtGuid.Text = rTxtGuid.Text + guid.ToLower() + "\r\n";
                }
            }

            this.lblLength.Text = String.Format("长度:{0}", guid.Length.ToString());
        }



        private void btnZh_Click(object sender, EventArgs e)
        {
            string[] str = new string[this.rTxtHanzi.Lines.Length];

            this.rTxtPinyin.Clear();

            string hanzi = string.Empty;
            string pinyin = string.Empty;

            for (int i = 0; i < this.rTxtHanzi.Lines.Length; i++)
            {
                //str[i] = this.rTxtHanzi.Lines[i];
                hanzi = this.rTxtHanzi.Lines[i];
                if (this.rbSzm.Checked)
                {
                    pinyin = pinyin + Util.Covert2Pinyin.GetSpellCode(hanzi) + "\r\n";
                }
                else
                {
                    pinyin = pinyin + Util.Covert2Pinyin.Convert(hanzi) + "\r\n";
                }
                
            }
            if (chk_lowercase.Checked)
            {
                this.rTxtPinyin.Text = pinyin.ToLower();
            }
            else
            {
                this.rTxtPinyin.Text = pinyin;
            }
            
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            Random rd = new Random();　　//无参即为使用系统时钟为种子
            //for (int i = 0; i < 10; i++)
            //{
               
            //    //Console.WriteLine(rd.Next().ToString());
            //    richTextBox1.Text = richTextBox1.Text + rd.Next().ToString() + "\r\n";
            //}
            int sl = Convert.ToInt32(txtScsl.Text.Trim());  // 生成数量
            int qj_s = Convert.ToInt32(txtSjqj_start.Text.Trim());  // 生成区间
            int qj_e = Convert.ToInt32(txtSjqj_end.Text.Trim());  // 生成区间

            string str_gen = string.Empty;

            for (int i = 0; i < sl; i++) 
            { 
                byte[] buffer = Guid.NewGuid().ToByteArray(); 
                int iSeed = BitConverter.ToInt32(buffer, 0); 
                Random random = new Random(iSeed);

                str_gen = string.Format("insert into <table>(col1, col2) values(val1,val2)");
                //richTextBox1.Text = richTextBox1.Text + rd.Next().ToString() + "\r\n"; 
                richTextBox1.Text = richTextBox1.Text + rd.Next(qj_s, qj_e).ToString() + "\r\n"; 

                
            }
        }

        private void btn_gen_md5_Click(object sender, EventArgs e)
        {
            string str_input = string.Empty;
            string str_md5s = string.Empty;
            string str_md5_16 = string.Empty;
            string str_md5_32 = string.Empty;
            string str_md5_64 = string.Empty;
            string str_prefix = string.Empty;


            str_input = this.txtInputStr.Text.Trim();
            str_md5_16 = StrJiami.MD5Encrypt16(str_input);
            str_md5_32 = StrJiami.MD5Encrypt32(str_input);
            str_md5_64 = StrJiami.MD5Encrypt64(str_input);


            // 16位MD5加密
            // 32位MD5加密
            // 64位MD5加密
            str_md5s = String.Format("Md5-16位：\r\n\t默认: {0} \r\n\t小写: {1} " +
                "\r\nMd5-32位：\r\n\t默认: {2}\r\n\t小写: {3}" +
                "\r\nMd5-64位：\r\n\t默认: {4}\r\n\t小写: {5}",
                    str_md5_16, str_md5_16.ToLower(),
                    str_md5_32, str_md5_32.ToLower(),
                    str_md5_64, str_md5_64.ToLower());

            this.txtMd5.Text = str_md5s;
            

        }

        private void btn_genName_Click(object sender, EventArgs e)
        {
            string name = string.Empty;
            string idCode = string.Empty;
            string gender = string.Empty;
            string brithday = string.Empty;
            string province = string.Empty;
            string age = string.Empty;

            txt_testdata_genname.Clear();

            int sl = Convert.ToInt32(txt_testData_xm_gennum.Text.Trim());  // 生成数量

            string str_gen = string.Empty;

            for (int i = 0; i < sl; i++)
            {
                //name = GenSfzCode.GenerateRandomSurName() + GenSfzCode.GenerateRandomName();
                name = GetNameHelper.GetName();

                idCode = GenSfzCode.GenerateIDNumber();
                //gender = GenSfzCode.GetRandomGender();

                //--

                gender = GenSfzCode.GetGenderFromIdCard(idCode);
                brithday = GenSfzCode.GetBirthdayFromIdCard(idCode);
                age = GenSfzCode.GetAgeFromIdBirthday(idCode).ToString();
                province = GenSfzCode.GetProvinceByIDCard(idCode);


                txt_testdata_genname.Text = txt_testdata_genname.Text + 
                    String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r\n", name, idCode, gender, brithday,age, province) ;                

            }

           
        }

        private void txt_testdata_genname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                this.txt_testdata_genname.SelectAll();
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }
    }
}
